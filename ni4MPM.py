#Density along MPM line of sight for separatrix density of 4E13. Each planar coil config is visible.
import numpy as np
import matplotlib.pyplot as plt
from eelab import Simulation
import os

# Define the function to process and plot data
def process_and_plot(folder, ni, Lc, phi, color):
    y_data = np.zeros(len(R))
    Lc_data = np.zeros(len(R))

    for i in range(len(R)):
        try:
            y_data[i] = ni(R[i], Z[i], phi)
            Lc_data[i] = Lc(R[i], Z[i], phi)
        except IndexError:
            y_data[i] = 0.
            Lc_data[i] = 0.
            print(f'Point R={R[i]:.2f} is outside of domain!')

    valid_indices = y_data > 0
    valid_R = np.array(R)[valid_indices]
    y_data = y_data[valid_indices]
    Lc_data = Lc_data[valid_indices]

    fig, ax = plt.subplots()
    ax.scatter(valid_R, y_data, label='Simulation', color=color)
    
    # Add shaded region where Lc > 80000
    ax.fill_between(valid_R, y_data, where=(Lc_data > 80000), color=color, alpha=0.2)

    ax.set_xlabel('R (cm)')
    ax.set_ylabel('Plasma Density (cm$^{-3}$)')
    grandparent_folder_name = os.path.basename(os.path.dirname(os.path.dirname(folder)))
    ax.set_title(f'Plasma Density along MPM Line of Sight with respect to Radial Distance - N4E13 - P5MW in {grandparent_folder_name}')

    return fig, ax, valid_R, y_data, Lc_data

# Parameters
phi = 15.2
Z = [16.7] * 108  # Set Z to 16.7
R = np.linspace(595, 628.8, 108)  # Linspace for R from 595 to 628.8 with twice as many points

# Specify the paths to the folders to process
folders = ['~/EIM-m750A-v2/P5MW/N4E13D.5_SP4', '~/EIM-m500A-v2/P5MW/N4E13D.5_SP4', '~/EIM-m250A/P5MW/N4E13D.5_C3', '~/std/P5MW/N4E13D.5_C3']

# Dictionary to store results for combined plot
data_results = {}

# List to store figure and axes objects
figs = []

# Define colors for different plots
colors = ['blue', 'green', 'red', 'orange']

# Process each specified folder
for i, folder in enumerate(folders):
    sim = Simulation(folder)
    ni = sim['ni']
    Lc = sim['Lc']
    color = colors[i]
    fig, ax, valid_R, y_data, Lc_data = process_and_plot(folder, ni, Lc, phi, color)
    figs.append((fig, ax))
    data_results[folder] = (valid_R, y_data, Lc_data, color)

# Combined plot
fig_combined, ax_combined = plt.subplots()
for key, (valid_R, y_data, Lc_data, color) in data_results.items():
    grandparent_folder_name = os.path.basename(os.path.dirname(os.path.dirname(key)))
    ax_combined.scatter(valid_R, y_data, label=f'{grandparent_folder_name} Simulation', color=color)
    
    # Add shaded region where Lc > 80000
    ax_combined.fill_between(valid_R, y_data, where=(Lc_data > 80000), color=color, alpha=0.2)
    
ax_combined.set_xlabel('R (cm)')
ax_combined.set_ylabel('Plasma Density (cm$^{-3}$)')
ax_combined.set_title('Plasma Density for Various Conditions along MPM Line of Sight - N4E13 - P5MW')
ax_combined.legend()
figs.append((fig_combined, ax_combined))

plt.show()
