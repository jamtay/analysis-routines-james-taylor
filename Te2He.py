#Temperature along helium beam line of sight for separatrix density of 2E13. Each planar coil config is visible.
from eelab import Simulation
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import os

# Define the function to fit
def exponential_growth(z, Te_0, lambda_Te, z0):
    return Te_0 * np.exp((z - z0) / lambda_Te)

# Define the function to process and plot data
def process_and_plot(folder, Te, Lc, distances, phi, color):
    value_at_point_Te = [Te(r, z, phi) for r, z in zip(Rlast, Zlast)]
    Lc_data = [Lc(r, z, phi) for r, z in zip(Rlast, Zlast)]
    
    distances = np.array(distances)
    Te_data = np.array(value_at_point_Te)

    # Improved initial guesses
    Te_0_initial = Te_data[0]
    lambda_Te_initial = (distances[-1] - distances[0]) / (np.log(Te_data[-1]) - np.log(Te_data[0])) if (np.log(Te_data[-1]) - np.log(Te_data[0])) != 0 else 1.0
    z0_initial = distances[0]

    initial_guess = [Te_0_initial, lambda_Te_initial, z0_initial]
    popt, _ = curve_fit(exponential_growth, distances, Te_data, p0=initial_guess)
    Te_0_opt, lambda_Te_opt, z0_opt = popt

    print(f"Folder: {folder}")
    print(f"Optimal Te_0: {Te_0_opt}")
    print(f"Optimal Growth Length (lambda_Te): {lambda_Te_opt}")
    print(f"Optimal Initial Distance (z0): {z0_opt}")

    distances_fit = np.linspace(min(distances), max(distances), 100)
    Te_fit = exponential_growth(distances_fit, Te_0_opt, lambda_Te_opt, z0_opt)

    # Interpolate Lc data to match distances_fit
    Lc_data_array = np.array(Lc_data)
    Lc_interpolated = np.interp(distances_fit, distances, Lc_data_array)

    fig, ax = plt.subplots()
    ax.scatter(distances, value_at_point_Te, label='Simulation', color=color)
    ax.plot(distances_fit, Te_fit, label='Fitted Curve', color=color)
    
    # Add shaded region where Lc > 80000
    ax.fill_between(distances_fit, Te_fit, where=(Lc_interpolated > 80000), color=color, alpha=0.2)

    ax.set_xlabel('Distance from Target (cm)')
    ax.set_ylabel('Electron Temperature (eV)')
    grandparent_folder_name = os.path.basename(os.path.dirname(os.path.dirname(folder)))
    ax.set_title(f'Electron Temperature along Helium Beam Line of Sight with respect to Distance from Target - N2E13 - P5MW in {grandparent_folder_name}')
    annotation_text = (f'Optimal $Te_0$: {Te_0_opt:.2f}\nOptimal Growth Length ($\\lambda_Te$): '
                       f'{lambda_Te_opt:.2f}\nOptimal Initial Distance ($z0$): {z0_opt:.2f}')
    ax.annotate(annotation_text, xy=(.25, 0.95), xycoords='axes fraction', fontsize=10,
                verticalalignment='top', bbox=dict(boxstyle="round,pad=0.3", edgecolor="black", facecolor="white"))

    return fig, ax, distances, value_at_point_Te, Te_0_opt, lambda_Te_opt, z0_opt, Lc_data_array

# Parameters
phi = 12.3
Rlast = [532.414286, 532.441633, 532.468981, 532.49633, 532.523678, 532.551028, 532.578378, 532.60573,
         532.633083, 532.660438, 532.687794, 532.715152, 532.742512, 532.769875, 532.79724, 532.824607,
         532.851977, 532.879351, 532.906727, 532.934107, 532.96149, 532.988877, 533.016268, 533.043663,
         533.071062, 533.098466, 533.125874, 533.153287, 533.180705, 533.208129, 533.235557, 533.262991,
         533.290431, 533.317877, 533.345329, 533.372787, 533.400252, 533.427723, 533.455201, 533.482687,
         533.510179, 533.537679, 533.565186, 533.592701, 533.620224, 533.647756, 533.675295, 533.702843,
         533.7304, 533.757966, 533.785541, 533.813125, 533.840718, 533.868322]
Zlast = [99.390902, 99.2338, 99.076696, 98.91959, 98.762481, 98.605367, 98.448247, 98.291121,
         98.133987, 97.976845, 97.819692, 97.662529, 97.505354, 97.348166, 97.190965, 97.033748,
         96.876515, 96.719265, 96.561997, 96.404709, 96.247402, 96.090073, 95.932722, 95.775347,
         95.617948, 95.460524, 95.303072, 95.145594, 94.988086, 94.830549, 94.67298, 94.515381,
         94.357748, 94.200082, 94.042379, 93.884642, 93.726866, 93.569053, 93.4112, 93.253307,
         93.095373, 92.937396, 92.779376, 92.62131, 92.4632, 92.305042, 92.146836, 91.988582,
         91.830277, 91.671922, 91.513514, 91.355053, 91.196538, 91.037967]

# Calculate distances from the target
R_target = Rlast[0]
Z_target = Zlast[0]

distances = [np.sqrt((r - R_target)**2 + (z - Z_target)**2) for r, z in zip(Rlast, Zlast)]

# Specify the paths to the folders to process
folders = ['~/EIM-m750A-v2/P5MW/N2E13D.5_SP4', '~/EIM-m500A-v2/P5MW/N2E13D.5_SP4', '~/EIM-m250A/P5MW/N2E13D.5_C3','~/std/P5MW/N2E13D.5_C3']

# Dictionary to store results for combined plot
data_results = {}

# List to store figure and axes objects
figs = []

# Define colors for different plots
colors = ['blue', 'green', 'red', 'orange']

# Process each specified folder
for i, folder in enumerate(folders):
    sim = Simulation(folder)
    Te = sim['Te']
    Lc = sim['Lc']
    color = colors[i]
    fig, ax, distances, value_at_point_Te, Te_0_opt, lambda_Te_opt, z0_opt, Lc_data = process_and_plot(folder, Te, Lc, distances, phi, color)
    figs.append((fig, ax))
    data_results[folder] = (distances, value_at_point_Te, Te_0_opt, lambda_Te_opt, z0_opt, Lc_data, color)

# Combined plot
fig_combined, ax_combined = plt.subplots()
for key, (distances, value_at_point_Te, Te_0_opt, lambda_Te_opt, z0_opt, Lc_data, color) in data_results.items():
    distances_fit = np.linspace(min(distances), max(distances), 100)
    Te_fit = exponential_growth(distances_fit, Te_0_opt, lambda_Te_opt, z0_opt)
    grandparent_folder_name = os.path.basename(os.path.dirname(os.path.dirname(key)))
    ax_combined.scatter(distances, value_at_point_Te, label=f'{grandparent_folder_name} Simulation', color=color)
    ax_combined.plot(distances_fit, Te_fit, color=color)
    
    # Interpolate Lc data to match distances_fit
    Lc_interpolated = np.interp(distances_fit, distances, Lc_data)
    
    # Add shaded region where Lc > 80000
    ax_combined.fill_between(distances_fit, Te_fit, where=(Lc_interpolated > 80000), color=color, alpha=0.2)
    
    annotation_text = f'{grandparent_folder_name} $\lambda_Te$: {lambda_Te_opt:.2f}'
    ax_combined.annotate(annotation_text, xy=(0.01, 0.85 - 0.05 * list(data_results.keys()).index(key)), xycoords='axes fraction',
                         fontsize=10, verticalalignment='top', bbox=dict(boxstyle="round,pad=0.3", edgecolor="black", facecolor="white"))

ax_combined.set_xlabel('Distance from Target (cm)')
ax_combined.set_ylabel('Electron Temperature (eV)')
ax_combined.set_title('Electron Temperature along Helium Beam Line of Sight with respect to Distance from Target - N2E13 - P5MW')
ax_combined.legend()
figs.append((fig_combined, ax_combined))

plt.show()
