#Field-line tracing from a series of points near the boundary of confined island remnant and near O-point of island. Density For 750 A PC current.
from eelab import Simulation
import matplotlib.pyplot as plt
import numpy as np

s=Simulation('~/EIM-m750A-v2/P5MW/N4E13D.5_SP4')
ni=s['ni']
Lc=s['Lc']


confined_points = [
    [523.13, 96.646, 12.3],
    [534.44, 98.307, 12.3],
    [546.716, 93.940, 12.3],
    [541.19, 92.369, 12.3],
    [529.589, 92.716, 12.3],

]

o_points = [
    [533.94, 95.25, 12.3],
    [536.01, 95.25, 12.3],
    [535.61, 94.24, 12.3],
    [531.39, 94.53, 12.3],
    [531, 96.3, 12.3],

]

    

for i in range(len(confined_points)):
    nifl = ni.fieldline(confined_points[i][0], confined_points[i][1], confined_points[i][2])
    plt.figure()
    ax1 = plt.gca()
    nifl.plot(color='red', label = 'ni')
    plt.title(f"Plasma Density along Field Line from confined point {confined_points[i][0], confined_points[i][1], confined_points[i][2]}")
    plt.legend(loc='upper left')
    #ax2=ax1.twinx()
    #Spfl.plot(color='blue', label = 'Sp')
    #Tefl.plot(color='blue', label = 'Te')
    #Gparafl.plot(color='blue', label = 'Gpara')
    #ax2.axhline(y=0, color = 'grey', linestyle='--')
    #Seecoolfl.plot(color='blue', label = 'Seecool')
    #ax2.set_xlim(-215,215)
    #plt.title(None)
    plt.legend()
    plt.figure()
    Lc.plot('rzslice', confined_points[i][2])
    plt.title(f"Connection Length - m750A Planar Coil Current for confined point {confined_points[i][0], confined_points[i][1], confined_points[i][2]}")
    plt.xlim(492,568)
    plt.ylim(60,125)
    ax = plt.gca()
    ax.scatter(confined_points[i][0], confined_points[i][1], c='b', s=20)
    

for i in range(len(o_points)):
    nifl = ni.fieldline(o_points[i][0], o_points[i][1], o_points[i][2])
    plt.figure()
    ax1 = plt.gca()
    nifl.plot(color='red', label = 'ni')
    plt.title(f"Plasma Density along Field Line around O point - {o_points[i][0], o_points[i][1], o_points[i][2]}")
    plt.legend(loc='upper left')
    #ax2=ax1.twinx()
    #Spfl.plot(color='blue', label = 'Sp')
    #Tefl.plot(color='blue', label = 'Te')
    #Gparafl.plot(color='blue', label = 'Gpara')
    #ax2.axhline(y=0, color = 'grey', linestyle='--')
    #Seecoolfl.plot(color='blue', label = 'Seecool')
    #ax2.set_xlim(-215,215)
    #plt.title(None)
    plt.legend()
    plt.figure()
    Lc.plot('rzslice', o_points[i][2])
    plt.title(f"Connection Length - m750A Planar Coil Current around O point - {o_points[i][0], o_points[i][1], o_points[i][2]}")
    plt.xlim(492,568)
    plt.ylim(60,125)
    ax = plt.gca()
    ax.scatter(o_points[i][0], o_points[i][1], c='b', s=20)
    
plt.show()
