#Field-line tracing from peak in ni4He density plot. Density, static pressure and particle source. For 750 A PC current.
from eelab import Simulation
import matplotlib.pyplot as plt
import numpy as np

s=Simulation('~/EIM-m750A-v2/P5MW/N4E13D.5_SP4')
ni=s['ni']
Sp=s['Sp']
Pstat=s['Pstat']
Te=s['Te']
Gpara=s['Gpara']
Seecool=s['Seecool']

nifl = ni.fieldline(533.208129, 94.830549, 12.3)
Spfl = Sp.fieldline(533.208129, 94.830549, 12.3)
Tefl = Te.fieldline(533.208129, 94.830549, 12.3)
Gparafl = Gpara.fieldline(533.208129, 94.830549, 12.3)
Seecoolfl = Seecool.fieldline(533.208129, 94.830549, 12.3)

plt.figure()

ax1 = plt.gca()
nifl.plot(color='red', label = 'ni')
#plt.title('Plasma Density and Electron Cooling Rate (Impurities)')
plt.legend(loc='upper left')
plt.title(None)
#ax2=ax1.twinx()
#Spfl.plot(color='blue', label = 'Sp')
#Tefl.plot(color='blue', label = 'Te')
#Gparafl.plot(color='blue', label = 'Gpara')
#ax2.axhline(y=0, color = 'grey', linestyle='--')
#Seecoolfl.plot(color='blue', label = 'Seecool')
#ax2.set_xlim(-215,215)
#plt.title(None)
plt.legend()

Pstatfl = Pstat.fieldline(533.208129, 94.830549, 12.3)

plt.figure()
Pstatfl.plot()
plt.xlim(-235,235)

x = nifl.grid.x1
y = nifl.grid.x2
z = nifl.grid.x3

# Initialize lists to store peak and dip indices, distances, and coordinates
peak_indices = []
dip_indices = []
Sp_indices = []
peak_distances = [90065, 90876, 91649]  # Distances for the three peaks
dip_distances = [90445, 91351, 92831]    # Distances for the three dips
Sp_distances = [89617, 90726.5, 91126, 91896]

# Initialize variables
ipeak = 0
idip = 0
iSp = 0
distpeak = 0
distdip = 0
distSp = 0

# Find the first peak
while distpeak < peak_distances[0]:
    distpeak += np.sqrt((x[ipeak + 1] - x[ipeak])**2 + (y[ipeak + 1] - y[ipeak])**2 + (z[ipeak + 1] - z[ipeak])**2)
    ipeak += 1
peak_indices.append(ipeak - 1)
print('Peak 1:', 'i =', ipeak, 'dist =', distpeak, 'x =', x[ipeak - 1], 'y =', y[ipeak - 1], 'z =', z[ipeak - 1])

# Find the first dip
while distdip < dip_distances[0]:
    distdip += np.sqrt((x[idip + 1] - x[idip])**2 + (y[idip + 1] - y[idip])**2 + (z[idip + 1] - z[idip])**2)
    idip += 1
dip_indices.append(idip - 1)
print('Dip 1:', 'i =', idip, 'dist =', distdip, 'x =', x[idip - 1], 'y =', y[idip - 1], 'z =', z[idip - 1])

# Find the second peak
distpeak = 0
ipeak = 0
while distpeak < peak_distances[1]:
    distpeak += np.sqrt((x[ipeak + 1] - x[ipeak])**2 + (y[ipeak + 1] - y[ipeak])**2 + (z[ipeak + 1] - z[ipeak])**2)
    ipeak += 1
peak_indices.append(ipeak - 1)
print('Peak 2:', 'i =', ipeak, 'dist =', distpeak, 'x =', x[ipeak - 1], 'y =', y[ipeak - 1], 'z =', z[ipeak - 1])

# Find the second dip
distdip = 0
idip = 0
while distdip < dip_distances[1]:
    distdip += np.sqrt((x[idip + 1] - x[idip])**2 + (y[idip + 1] - y[idip])**2 + (z[idip + 1] - z[idip])**2)
    idip += 1
dip_indices.append(idip - 1)
print('Dip 2:', 'i =', idip, 'dist =', distdip, 'x =', x[idip - 1], 'y =', y[idip - 1], 'z =', z[idip - 1])

# Find the third peak
distpeak = 0
ipeak = 0
while distpeak < peak_distances[2]:
    distpeak += np.sqrt((x[ipeak + 1] - x[ipeak])**2 + (y[ipeak + 1] - y[ipeak])**2 + (z[ipeak + 1] - z[ipeak])**2)
    ipeak += 1
peak_indices.append(ipeak - 1)
print('Peak 3:', 'i =', ipeak, 'dist =', distpeak, 'x =', x[ipeak - 1], 'y =', y[ipeak - 1], 'z =', z[ipeak - 1])

# Find the third dip
distdip = 0
idip = 0
while distdip < dip_distances[2]:
    distdip += np.sqrt((x[idip + 1] - x[idip])**2 + (y[idip + 1] - y[idip])**2 + (z[idip + 1] - z[idip])**2)
    idip += 1
dip_indices.append(idip - 1)
print('Dip 3:', 'i =', idip, 'dist =', distdip, 'x =', x[idip - 1], 'y =', y[idip - 1], 'z =', z[idip - 1])

# Find the first Sp peak
while distSp < Sp_distances[0]:
    distSp += np.sqrt((x[iSp + 1] - x[iSp])**2 + (y[iSp + 1] - y[iSp])**2 + (z[iSp + 1] - z[iSp])**2)
    iSp += 1
Sp_indices.append(iSp - 1)
print('Sp Peak 1:', 'i =', iSp, 'dist =', distSp, 'x =', x[iSp - 1], 'y =', y[iSp - 1], 'z =', z[iSp - 1])

# Find the second Sp peak
distSp = 0
iSp = 0
while distSp < Sp_distances[1]:
    distSp += np.sqrt((x[iSp + 1] - x[iSp])**2 + (y[iSp + 1] - y[iSp])**2 + (z[iSp + 1] - z[iSp])**2)
    iSp += 1
Sp_indices.append(iSp - 1)
print('Sp Peak 2:', 'i =', iSp, 'dist =', distSp, 'x =', x[iSp - 1], 'y =', y[iSp - 1], 'z =', z[iSp - 1])

# Find the third Sp peak
distSp = 0
iSp = 0
while distSp < Sp_distances[2]:
    distSp += np.sqrt((x[iSp + 1] - x[iSp])**2 + (y[iSp + 1] - y[iSp])**2 + (z[iSp + 1] - z[iSp])**2)
    iSp += 1
Sp_indices.append(iSp - 1)
print('Sp Peak 3:', 'i =', iSp, 'dist =', distSp, 'x =', x[iSp - 1], 'y =', y[iSp - 1], 'z =', z[iSp - 1])

# Find the fourth Sp peak
distSp = 0
iSp = 0
while distSp < Sp_distances[3]:
    distSp += np.sqrt((x[iSp + 1] - x[iSp])**2 + (y[iSp + 1] - y[iSp])**2 + (z[iSp + 1] - z[iSp])**2)
    iSp += 1
Sp_indices.append(iSp - 1)
print('Sp Peak 4:', 'i =', iSp, 'dist =', distSp, 'x =', x[iSp - 1], 'y =', y[iSp - 1], 'z =', z[iSp - 1])


# Calculate r, z, and phi for all peaks and dips
for i, peak_index in enumerate(peak_indices):
    rpeak = np.sqrt(x[peak_index]**2 + y[peak_index]**2)
    zpeak = z[peak_index]
    phipeak = np.arctan2(y[peak_index], x[peak_index])
    print(f'Peak {i+1}: r = {rpeak}, z = {zpeak}, phi = {phipeak*180/np.pi}')

for i, dip_index in enumerate(dip_indices):
    rdip = np.sqrt(x[dip_index]**2 + y[dip_index]**2)
    zdip = z[dip_index]
    phidip = np.arctan2(y[dip_index], x[dip_index])
    print(f'Dip {i+1}: r = {rdip}, z = {zdip}, phi = {phidip*180/np.pi}')

for i, Sp_index in enumerate(Sp_indices):
    rSp = np.sqrt(x[Sp_index]**2 + y[Sp_index]**2)
    zSp = z[Sp_index]
    phiSp = np.arctan2(y[Sp_index], x[Sp_index])
    print(f'Sp Peak {i+1}: r = {rSp}, z = {zSp}, phi = {phiSp*180/np.pi}')


plt.show()
