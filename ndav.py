#Downstream Density for each simulation
from eelab import Simulation
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from eelab.dtypes import VolData

phi = 12.3

s2=Simulation('~/EIM-m750A-v2/P5MW/N2E13D.5_SP4')
ndav2=s2['ndav']

s3=Simulation('~/EIM-m500A-v2/P5MW/N2E13D.5_SP4')
ndav3=s3['ndav']

s4=Simulation('~/EIM-m250A/P5MW/N2E13D.5_C3')
ndav4=s4['ndav']

s5=Simulation('~/std/P5MW/N2E13D.5_C3')
ndav5=s5['ndav']

plt.figure()
ndav2.plot()

plt.figure()
ndav3.plot()

plt.figure()
ndav4.plot()

plt.figure()
ndav5.plot()

plt.show()
