#Ion Temperature profile along Helium Beam Line of Sight for each configuration. With N2E13 separatrix Density.
from eelab import Simulation
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import os

# Define the function to fit
def exponential_decay(z, Ti_0, lambda_Ti, z0):
    return Ti_0 * np.exp(-(z - z0) / lambda_Ti)

# Define the function to process and plot data
def process_and_plot(folder, Ti, Rlast, Zlast, phi):
    value_at_point_Ti = [Ti(r, z, phi) for r, z in zip(Rlast, Zlast)]
    
    z_data = np.array(Zlast)
    Ti_data = np.array(value_at_point_Ti)

    # Improved initial guesses
    Ti_0_initial = Ti_data[0]
    lambda_Ti_initial = (z_data[-1] - z_data[0]) / (np.log(Ti_data[0]) - np.log(Ti_data[-1])) if (np.log(Ti_data[0]) - np.log(Ti_data[-1])) != 0 else 1.0
    z0_initial = z_data[0]

    initial_guess = [Ti_0_initial, lambda_Ti_initial, z0_initial]
    popt, _ = curve_fit(exponential_decay, z_data, Ti_data, p0=initial_guess)
    Ti_0_opt, lambda_Ti_opt, z0_opt = popt

    print(f"Folder: {folder}")
    print(f"Optimal Ti_0: {Ti_0_opt}")
    print(f"Optimal Decay Length (lambda_Ti): {lambda_Ti_opt}")
    print(f"Optimal Initial Vertical Position (z0): {z0_opt}")

    Z_fit = np.linspace(min(z_data), max(z_data), 100)
    Ti_fit = exponential_decay(Z_fit, Ti_0_opt, lambda_Ti_opt, z0_opt)

    fig, ax = plt.subplots()
    ax.scatter(Zlast, value_at_point_Ti, label='Simulation')
    ax.plot(Z_fit, Ti_fit, label='Fitted Curve', color='red')
    ax.set_xlabel('Vertical Distance (cm)')
    ax.set_ylabel('Ion Temperature (eV)')
    grandparent_folder_name = os.path.basename(os.path.dirname(os.path.dirname(folder)))
    ax.set_title(f'Ion Temperature along Helium Beam Line of Sight with respect to Vertical Distance - N2E13 - P2.75MW in {grandparent_folder_name}')
    annotation_text = (f'Optimal $Ti_0$: {Ti_0_opt:.2f}\nOptimal Decay Length ($\\lambda_Ti$): '
                       f'{lambda_Ti_opt:.2f}\nOptimal Initial Vertical Position ($z0$): {z0_opt:.2f}')
    ax.annotate(annotation_text, xy=(.25, 0.95), xycoords='axes fraction', fontsize=10,
                verticalalignment='top', bbox=dict(boxstyle="round,pad=0.3", edgecolor="black", facecolor="white"))

    return fig, ax, Zlast, value_at_point_Ti, Ti_0_opt, lambda_Ti_opt, z0_opt

# Parameters
phi = 12.3
Rlast = [532.414286, 532.441633, 532.468981, 532.49633, 532.523678, 532.551028, 532.578378, 532.60573,
         532.633083, 532.660438, 532.687794, 532.715152, 532.742512, 532.769875, 532.79724, 532.824607,
         532.851977, 532.879351, 532.906727, 532.934107, 532.96149, 532.988877, 533.016268, 533.043663,
         533.071062, 533.098466, 533.125874, 533.153287, 533.180705, 533.208129, 533.235557, 533.262991,
         533.290431, 533.317877, 533.345329, 533.372787, 533.400252, 533.427723, 533.455201, 533.482687,
         533.510179, 533.537679, 533.565186, 533.592701, 533.620224, 533.647756, 533.675295, 533.702843,
         533.7304, 533.757966, 533.785541, 533.813125, 533.840718, 533.868322]
Zlast = [99.390902, 99.2338, 99.076696, 98.91959, 98.762481, 98.605367, 98.448247, 98.291121,
         98.133987, 97.976845, 97.819692, 97.662529, 97.505354, 97.348166, 97.190965, 97.033748,
         96.876515, 96.719265, 96.561997, 96.404709, 96.247402, 96.090073, 95.932722, 95.775347,
         95.617948, 95.460524, 95.303072, 95.145594, 94.988086, 94.830549, 94.67298, 94.515381,
         94.357748, 94.200082, 94.042379, 93.884642, 93.726866, 93.569053, 93.4112, 93.253307,
         93.095373, 92.937396, 92.779376, 92.62131, 92.4632, 92.305042, 92.146836, 91.988582,
         91.830277, 91.671922, 91.513514, 91.355053, 91.196538, 91.037967]

# Specify the paths to the folders to process
folders = ['~/EIM-m750A-v2/P5MW/N2E13D.5_SP4', '~/EIM-m500A-v2/P5MW/N2E13D.5_SP4', '~/EIM-m250A/P5MW/N2E13D.5_C3','~/std/P5MW/N2E13D.5_C3']

# Dictionary to store results for combined plot
data_results = {}

# List to store figure and axes objects
figs = []

# Process each specified folder
for folder in folders:
    sim = Simulation(folder)
    Ti = sim['Ti']
    fig, ax, Zlast, value_at_point_Ti, Ti_0_opt, lambda_Ti_opt, z0_opt = process_and_plot(folder, Ti, Rlast, Zlast, phi)
    figs.append((fig, ax))
    data_results[folder] = (Zlast, value_at_point_Ti, Ti_0_opt, lambda_Ti_opt, z0_opt)

# Combined plot
fig_combined, ax_combined = plt.subplots()
for key, (Zlast, value_at_point_Ti, Ti_0_opt, lambda_Ti_opt, z0_opt) in data_results.items():
    Z_fit = np.linspace(min(Zlast), max(Zlast), 100)
    Ti_fit = exponential_decay(Z_fit, Ti_0_opt, lambda_Ti_opt, z0_opt)
    grandparent_folder_name = os.path.basename(os.path.dirname(os.path.dirname(key)))
    ax_combined.scatter(Zlast, value_at_point_Ti, label=f'{grandparent_folder_name} Simulation')
    ax_combined.plot(Z_fit, Ti_fit)
    annotation_text = f'{grandparent_folder_name} $\lambda_Ti$: {lambda_Ti_opt:.2f}'
    ax_combined.annotate(annotation_text, xy=(0.735, 0.75 - 0.075 * list(data_results.keys()).index(key)), xycoords='axes fraction',
                         fontsize=10, verticalalignment='top', bbox=dict(boxstyle="round,pad=0.3", edgecolor="black", facecolor="white"))

ax_combined.set_xlabel('Vertical Distance (cm)')
ax_combined.set_ylabel('Ion Temperature (eV)')
ax_combined.set_title('Ion Temperature along Helium Beam Line of Sight with respect to Vertical Distance - N2E13 - P5MW')
ax_combined.legend()
figs.append((fig_combined, ax_combined))

plt.show()
