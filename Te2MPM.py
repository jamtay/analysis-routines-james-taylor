#Temperature along MPM line of sight for separatrix density of 2E13. Each planar coil config is visible.
from eelab import Simulation
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import os

# Define the function to fit
def exponential_decay(R, Te_0, lambda_Te, R0):
    return Te_0 * np.exp(-(R - R0) / lambda_Te)

# Define the function to process and plot data
def process_and_plot(folder, Te, Lc, phi, color):
    profile = np.zeros(len(R))
    Lc_data = np.zeros(len(R))
    
    for i in range(len(R)):
        try:
            profile[i] = Te(R[i], Z[i], phi)
            Lc_data[i] = Lc(R[i], Z[i], phi)
        except IndexError:
            profile[i] = 0.
            print(f'Point R={R[i]:.2f} is outside of domain!')

    Te_data = profile
    valid_indices = Te_data > 0

    if np.sum(valid_indices) < 2:  # Ensure there are enough points to fit the curve
        print(f"Not enough valid data points found for folder {folder}. Skipping...")
        return None, None, None, None, None, None, None, None

    valid_R = np.array(R)[valid_indices]
    valid_Z = np.array(Z)[valid_indices]
    Te_data = Te_data[valid_indices]
    Lc_data = Lc_data[valid_indices]

    # Improved initial guesses
    Te_0_initial = Te_data[0]
    lambda_Te_initial = (valid_R[-1] - valid_R[0]) / (np.log(Te_data[0]) - np.log(Te_data[-1])) if (np.log(Te_data[0]) - np.log(Te_data[-1])) != 0 else 1.0
    R0_initial = valid_R[0]

    initial_guess = [Te_0_initial, lambda_Te_initial, R0_initial]
    popt, _ = curve_fit(exponential_decay, valid_R, Te_data, p0=initial_guess)
    Te_0_opt, lambda_Te_opt, R0_opt = popt

    print(f"Folder: {folder}")
    print(f"Optimal Te_0: {Te_0_opt}")
    print(f"Optimal Decay Length (lambda_Te): {lambda_Te_opt}")
    print(f"Optimal Initial R (R0): {R0_opt}")

    R_fit = np.linspace(min(valid_R), max(valid_R), 100)
    Te_fit = exponential_decay(R_fit, Te_0_opt, lambda_Te_opt, R0_opt)

    fig, ax = plt.subplots()
    ax.scatter(valid_R, Te_data, label='Simulation', color=color)
    ax.plot(R_fit, Te_fit, label='Fitted Curve', color=color)
    
    # Add shaded region where Lc > 80000
    ax.fill_between(valid_R, Te_data, where=(Lc_data > 80000), color=color, alpha=0.2)

    ax.set_xlabel('R (cm)')
    ax.set_ylabel('Electron Temperature (eV)')
    grandparent_folder_name = os.path.basename(os.path.dirname(os.path.dirname(folder)))
    ax.set_title(f'Electron Temperature along MPM Line of Sight with respect to Radial Distance - N2E13 - P5MW in {grandparent_folder_name}')
    annotation_text = (f'Optimal $Te_0$: {Te_0_opt:.2f}\nOptimal Decay Length ($\\lambda_Te$): '
                       f'{lambda_Te_opt:.2f}\nOptimal Initial R ($R0$): {R0_opt:.2f}')
    ax.annotate(annotation_text, xy=(.25, 0.95), xycoords='axes fraction', fontsize=10,
                verticalalignment='top', bbox=dict(boxstyle="round,pad=0.3", edgecolor="black", facecolor="white"))

    return fig, ax, valid_R, Te_data, Te_0_opt, lambda_Te_opt, R0_opt, Lc_data

# Parameters
phi = 15.2
Z = [16.7] * 108  # Set Z to -16.7
R = np.linspace(628.8, 595, 108)  # Linspace for R from 628.8 to 595 with twice as many points

# Specify the paths to the folders to process
folders = ['~/EIM-m750A-v2/P5MW/N2E13D.5_SP4', '~/EIM-m500A-v2/P5MW/N2E13D.5_SP4', '~/EIM-m250A/P5MW/N2E13D.5_C3','~/std/P5MW/N2E13D.5_C3']

# Dictionary to store results for combined plot
data_results = {}

# List to store figure and axes objects
figs = []

# Define colors for different plots
colors = ['blue', 'green', 'red', 'orange']

# Process each specified folder
for i, folder in enumerate(folders):
    sim = Simulation(folder)
    Te = sim['Te']
    Lc = sim['Lc']
    color = colors[i]
    fig, ax, valid_R, value_at_point_Te, Te_0_opt, lambda_Te_opt, R0_opt, Lc_data = process_and_plot(folder, Te, Lc, phi, color)
    if fig is None:
        continue
    figs.append((fig, ax))
    data_results[folder] = (valid_R, value_at_point_Te, Te_0_opt, lambda_Te_opt, R0_opt, Lc_data, color)

# Combined plot
fig_combined, ax_combined = plt.subplots()
for key, (valid_R, value_at_point_Te, Te_0_opt, lambda_Te_opt, R0_opt, Lc_data, color) in data_results.items():
    R_fit = np.linspace(min(valid_R), max(valid_R), 100)
    Te_fit = exponential_decay(R_fit, Te_0_opt, lambda_Te_opt, R0_opt)
    grandparent_folder_name = os.path.basename(os.path.dirname(os.path.dirname(key)))
    ax_combined.scatter(valid_R, value_at_point_Te, label=f'{grandparent_folder_name} Simulation', color=color)
    ax_combined.plot(R_fit, Te_fit, color=color)
    
    # Add shaded region where Lc > 80000
    ax_combined.fill_between(valid_R, value_at_point_Te, where=(Lc_data > 80000), color=color, alpha=0.2)
    
    annotation_text = f'{grandparent_folder_name} $\lambda_Te$: {lambda_Te_opt:.2f}'
    ax_combined.annotate(annotation_text, xy=(0.85, 0.85 - 0.05 * list(data_results.keys()).index(key)), xycoords='axes fraction',
                         fontsize=10, verticalalignment='top', bbox=dict(boxstyle="round,pad=0.3", edgecolor="black", facecolor="white"))

ax_combined.set_xlabel('R (cm)')
ax_combined.set_ylabel('Electron Temperature (eV)')
ax_combined.set_title('Electron Temperature along MPM Line of Sight with respect to Radial Distance - N2E13 - P5MW')
ax_combined.legend()
figs.append((fig_combined, ax_combined))

plt.show()
