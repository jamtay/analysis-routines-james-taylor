#Checking to see where points cross poloidal cells. Explains spikes in plot.
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from eelab import Simulation
import os

phi = 12.3

R = [532.414286, 532.441633, 532.468981, 532.49633, 532.523678, 532.551028, 532.578378, 532.60573,
      532.633083, 532.660438, 532.687794, 532.715152, 532.742512, 532.769875, 532.79724, 532.824607,
      532.851977, 532.879351, 532.906727, 532.934107, 532.96149, 532.988877, 533.016268, 533.043663,
      533.071062, 533.098466, 533.125874, 533.153287, 533.180705, 533.208129, 533.235557, 533.262991,
      533.290431, 533.317877, 533.345329, 533.372787, 533.400252, 533.427723, 533.455201, 533.482687,
      533.510179, 533.537679, 533.565186, 533.592701, 533.620224, 533.647756, 533.675295, 533.702843,
      533.7304, 533.757966, 533.785541, 533.813125, 533.840718, 533.868322]

Z = [99.390902, 99.2338, 99.076696, 98.91959, 98.762481, 98.605367, 98.448247, 98.291121,
      98.133987, 97.976845, 97.819692, 97.662529, 97.505354, 97.348166, 97.190965, 97.033748,
      96.876515, 96.719265, 96.561997, 96.404709, 96.247402, 96.090073, 95.932722, 95.775347,
      95.617948, 95.460524, 95.303072, 95.145594, 94.988086, 94.830549, 94.67298, 94.515381,
      94.357748, 94.200082, 94.042379, 93.884642, 93.726866, 93.569053, 93.4112, 93.253307,
      93.095373, 92.937396, 92.779376, 92.62131, 92.4632, 92.305042, 92.146836, 91.988582,
      91.830277, 91.671922, 91.513514, 91.355053, 91.196538, 91.037967]

# Specify the paths to the folders to process
folders = ['~/EIM-m750A-v2/P5MW/N4E13D.5_SP4', '~/EIM-m500A-v2/P5MW/N4E13D.5_SP4', '~/EIM-m250A/P5MW/N4E13D.5_C3','~/std/P5MW/N4E13D.5_C3']

# Process each specified folder
for i, folder in enumerate(folders):
    sim = Simulation(folder)
    ni = sim['ni']
    Lc = sim['Lc']
    plt.figure()
    ni.plot('rzslice',phi)
    ax= plt.gca()
    ax.scatter(R,Z,color='r', s=2.5)
    grandparent_folder_name = os.path.basename(os.path.dirname(os.path.dirname(folder)))
    ax.set_title(f'Plasma Density along Helium Beam Line of Sight for - N4E13 - P5MW in {grandparent_folder_name}')
    plt.figure()
    ax2= plt.gca()
    ni.plot('rzslice',12.3,edgecolor='k')
    ax2.scatter(R,Z,color='r', s=2.5)
    ax2.set_title(f'Plasma Density along Helium Beam Line of Sight for - N4E13 - P5MW in {grandparent_folder_name}')


plt.show()
