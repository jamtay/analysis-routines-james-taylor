#Langmuir Probe locations on density toroidal cross-section
import os
from copy import deepcopy
import numpy as np
import matplotlib.pylab as plt
import matplotlib.colors as c
from eelab import Simulation
from eelab.dtypes import VolData

s = Simulation('~/std/P5MW/N2E13D.5_C3')
Te = s['Te']
ni = s['ni']
Lc = s['Lc']
Sp = s['Sp']

LPL = np.array([
   [865.719, -5452.778, -1023.417],
   [861.917, -5428.400, -1019.385],
   [858.116, -5404.021, -1015.353],
   [854.315, -5379.643, -1011.322],
   [850.514, -5355.265, -1007.290],
   [839.110, -5282.130,  -995.197],
   [835.309, -5257.752,  -991.166],
   [831.508, -5233.374,  -987.135],
   [827.706, -5208.995,  -983.104],
   [823.905, -5184.617,  -979.074],
   [918.333, -5435.426, -1018.337],
   [914.314, -5411.072, -1014.370],
   [910.295, -5386.719, -1010.403],
   [906.276, -5362.365, -1006.436],
   [894.220, -5289.303,  -994.535],
   [890.201, -5264.949,  -990.569],
   [886.182, -5240.595,  -986.602],
   [882.164, -5216.241,  -982.636],
   [878.145, -5191.887,  -978.669]]) /1000.

RLPL =np.sqrt(LPL[:,0]**2 + LPL[:,1]**2)*100
ZLPL = LPL[:,2]*100
DZLPL = [x + 4 for x in ZLPL]
phiLPL = -(np.arctan2(LPL[:,1],LPL[:,0])*180/np.pi + 72)

print(phiLPL)

LPU = np.array([
  [2504.700, -4920.250, 1023.425],
  [2493.425, -4898.300, 1019.350],
  [2482.175, -4876.325, 1015.350],
  [2470.900, -4854.375, 1011.350],
  [2459.650, -4832.425, 1007.275],
  [2425.900, -4766.550,  995.200],
  [2414.650, -4744.600,  991.200],
  [2403.400, -4722.625,  987.100],
  [2392.150, -4700.675,  983.100],
  [2380.900, -4678.725,  979.100],
  [2451.925, -4937.125, 1018.350],
  [2440.850, -4915.075, 1014.375],
  [2429.800, -4893.000, 1010.400],
  [2418.725, -4870.950, 1006.425],
  [2385.525, -4804.750,  994.500],
  [2374.475, -4782.675,  990.600],
  [2363.400, -4760.625,  986.600],
  [2352.325, -4738.550,  982.625],
  [2341.275, -4716.475,  978.675]]) /1000

RLPU =np.sqrt(LPU[:,0]**2 + LPU[:,1]**2)*100
ZLPU = LPU[:,2]*100
DZLPU = [x + 4 for x in ZLPU]
phiLPU = np.arctan2(LPU[:,1],LPU[:,0])*180/np.pi + 72

print(phiLPU)

plt.figure()
ax = plt.gca()
ni.plot('rzslice', 8.5)
ax.plot(RLPU, ZLPU, c='r', ls='-')
ax.set_title('ni with Upper Langmuir Probe Path - std Planar Coil Current')

#plt.figure()
#ax = plt.gca()
#ni.plot('rzslice', 8.5)
#ax.plot(RLPL, ZLPL, c='r', ls='-')
#ax.set_title('ni with Lower Langmuir Probe Path - std Planar Coil Current')

plt.show()
