#One of the original density plots. Not so relevant now.
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from eelab import Simulation
import os

phi = 12.3

def gaussian(x, a, b, c):
    """ Gaussian function used for curve fitting. """
    return a * np.exp(-((x - b)**2) / (2 * c**2))

def fit_and_plot(ax, z_axis, y_data, title, label, point_color, line_color):
    """ Fits Gaussian functions to the data and adds it to the plot. """
    split_index = np.argmax(y_data)
    split_point_z = z_axis[split_index]

    # Segments for fitting
    z1, y1 = z_axis[:split_index], y_data[:split_index]
    z2, y2 = z_axis[split_index:], y_data[split_index:]

    # Fit Gaussians to each segment
    if len(z1) > 2 and len(y1) > 2:
        popt1, _ = curve_fit(gaussian, z1, y1, p0=[max(y1), np.mean(z1), np.std(z1)], maxfev=1000000)
        ax.plot(np.append(z1, z2[0]), gaussian(np.append(z1, z2[0]), *popt1), color=line_color)
    else:
        popt1 = [np.nan, np.nan, np.nan]  # Placeholder if fit not performed

    if len(z2) > 2 and len(y2) > 2:
        popt2, _ = curve_fit(gaussian, z2, y2, p0=[max(y2), np.mean(z2), np.std(z2)], maxfev=1000000)
        ax.plot(z2, gaussian(z2, *popt2), color=line_color)
    else:
        popt2 = [np.nan, np.nan, np.nan]  # Placeholder if fit not performed

    # Plot fitted curves and points
    ax.scatter(z_axis, y_data, color=point_color, alpha=0.5, label=f'{label} - Data Points')  # Plot data points
    if not np.isnan(popt1[0]) and not np.isnan(popt2[0]):
        ax.vlines(split_point_z, gaussian(split_point_z, *popt1), gaussian(split_point_z, *popt2), color=line_color, linestyle='--')

    ax.set_xlabel('Vertical Distance (cm)')
    ax.set_ylabel('Plasma Density (cm$^{-3}$)')
    ax.set_title(title)
    ax.legend()

# Parameters
phi = 12.3
Rlast = [532.414286, 532.441633, 532.468981, 532.49633, 532.523678, 532.551028, 532.578378, 532.60573,
         532.633083, 532.660438, 532.687794, 532.715152, 532.742512, 532.769875, 532.79724, 532.824607,
         532.851977, 532.879351, 532.906727, 532.934107, 532.96149, 532.988877, 533.016268, 533.043663,
         533.071062, 533.098466, 533.125874, 533.153287, 533.180705, 533.208129, 533.235557, 533.262991,
         533.290431, 533.317877, 533.345329, 533.372787, 533.400252, 533.427723, 533.455201, 533.482687,
         533.510179, 533.537679, 533.565186, 533.592701, 533.620224, 533.647756, 533.675295, 533.702843,
         533.7304, 533.757966, 533.785541, 533.813125, 533.840718, 533.868322]
Zlast = [99.390902, 99.2338, 99.076696, 98.91959, 98.762481, 98.605367, 98.448247, 98.291121,
         98.133987, 97.976845, 97.819692, 97.662529, 97.505354, 97.348166, 97.190965, 97.033748,
         96.876515, 96.719265, 96.561997, 96.404709, 96.247402, 96.090073, 95.932722, 95.775347,
         95.617948, 95.460524, 95.303072, 95.145594, 94.988086, 94.830549, 94.67298, 94.515381,
         94.357748, 94.200082, 94.042379, 93.884642, 93.726866, 93.569053, 93.4112, 93.253307,
         93.095373, 92.937396, 92.779376, 92.62131, 92.4632, 92.305042, 92.146836, 91.988582,
         91.830277, 91.671922, 91.513514, 91.355053, 91.196538, 91.037967]

# Specify the paths to the folders to process
folders = ['~/EIM-m750A-v2/P5MW/N2E13D.5_SP4', '~/EIM-m500A/P5MW/N2E13D.5_C3', '~/EIM-m250A/P5MW/N2E13D.5_C3','~/std/P5MW/N2E13D.5_C3']

# Dictionary to store results for combined plot
data_results = {}

# List to store figure and axes objects
figs = []

# Define colors for different plots
colors = ['blue', 'green', 'red', 'orange']

# Process each specified folder
for i, folder in enumerate(folders):
    sim = Simulation(folder)
    ni = sim['ni']
    z_axis = Zlast
    y_data = [ni(r, z, phi) for r, z in zip(Rlast, Zlast)]
    folder_name = os.path.basename(os.path.dirname(os.path.dirname(folder)))
    fig, ax = plt.subplots()
    fit_and_plot(ax, z_axis, y_data, f'Plasma Density for {folder_name}', folder_name, colors[i], colors[i])
    figs.append((fig, ax))
    data_results[folder] = (z_axis, y_data, folder_name)

# Combined plot
fig_combined, ax_combined = plt.subplots()
for i, (key, (z_axis, y_data, folder_name)) in enumerate(data_results.items()):
    fit_and_plot(ax_combined, z_axis, y_data, 'Combined Plot', folder_name, colors[i], colors[i])

ax_combined.set_xlabel('Vertical Distance (cm)')
ax_combined.set_ylabel('Plasma Density (cm$^{-3}$)')
ax_combined.set_title('Plasma Density for Various Conditions along Helium Beam Line of Sight - N2E13 - P5MW')
ax_combined.legend(loc='upper left', fontsize='small')

plt.show()
