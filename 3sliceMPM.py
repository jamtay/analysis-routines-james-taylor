#produces 3 toroidal slices of density, ionisation source and neutral mean free path with MPM coordinates.
import os
from copy import deepcopy
import numpy as np
import matplotlib.pylab as plt
import matplotlib.colors as c
from eelab import Simulation
from eelab.dtypes import VolData

phi = 15.2

s = Simulation('~/EIM-m750A-v2/P5MW/N4E13D.5_SP4')
Te = s['Te']
ni = s['ni']
Lc = s['Lc']
Sp = s['Sp']

# New R and Z coordinates
R = np.linspace(595, 628.8, 54)
Z = [16.7] * 54

# Calculate ionization and recombination sources separately with AMJUEL rates
spath = '/toks/work/flr/SOLPS/solps5.0/src/Braams/b2/runs/HDM/28903-2400-divIId-D+N-D-096x036-TEST/testcase-2-DONLY-BC-RERUN/'
dpath = '/afs/ipp/u/flr/python/amd/eirdb/'
import atomdat.eirdb.reactions_database as rdb; rdb = rdb.REACTIONS_DATABASE()
rdb.open_database('/u/jamtay/atomdat/atomdat/eirdb/amjuel_hydhel_database.json')

reactions = [
    'AMJUEL,4,2_1_5',  # Sion_p
    'AMJUEL,10,2_1_5',  # Sion_E
    'AMJUEL,4,2_1_8',  # Srec_p
    'AMJUEL,10,2_1_8',  # Srec_E
    'AMJUEL,3,3_1_8',  # SCX
]

rd = []
for item in reactions:
    rdb.select_reaction(item)
    rd.append(deepcopy(rdb))

nr = 5

rates = np.zeros((nr, len(Te.values)))
for j in range(nr):
    rates[j, :] = rd[j].reaction(ni.values * 1000000, Te.values, Te.values)

sip = rates[0, :]
sc = rates[4, :]

v_th = []
lambda_ = []

for i in range(len(rates[0, :] * ni.values * ni.values)):
    v_th.append(np.sqrt(3 * Te.values[i] * 1.6E-19 / 1.66E-27))
   
for i in range(len(rates[0, :] * ni.values * ni.values)): 
    lambda_.append(v_th[i] / ((ni.values[i] * 1000000) * np.sqrt(sip[i] * sc[i])))

lambda_array = np.array(lambda_)

mfp = VolData(grid=Te.grid, values=lambda_array, symbol='lambda_mfp', name='Ionisation Mean Free Path', rank='scalar')

plt.figure()
mfp.plot('rzslice', 15.2, norm=c.LogNorm(vmin=0.005, vmax=2))
ax = plt.gca()
Lc.plot('rzslice', 15.2, ax=ax, alpha=0.065)

ax.plot(R, Z, c='r', ls='-')
ax.set_title('Ionisation Mean Free Path with Connection Length Overlay - std Planar Coil Current')

plt.figure()
ni.plot('rzslice', 15.2)
plt.title('Density with Connection Length Overlay - std Planar Coil Current')
ax = plt.gca()
Lc.plot('rzslice', 15.2, ax=ax, alpha=0.075)

ax.plot(R, Z, c='r', ls='-')
ax.set_title('Density with Connection Length Overlay - std Planar Coil Current')

plt.figure()
Sp.plot('rzslice', 15.2, norm=c.LogNorm(vmin=1E16, vmax=1E18))
ax = plt.gca()
Lc.plot('rzslice', 15.2, ax=ax, alpha=0.075)

ax.plot(R, Z, c='r', ls='-')
ax.set_title('Sp with Connection Length Overlay - std Planar Coil Current')

plt.figure()
Lc.plot('rzslice', 15.2)
ax = plt.gca()
ax.plot(R,Z,c='r',ls='-')

plt.show()
