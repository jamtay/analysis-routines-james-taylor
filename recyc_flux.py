#Total recycling flux and power loss (impurities) as a function of planar coil current. 
import numpy as np
import matplotlib.pyplot as plt
from eelab import Simulation
import os

PC_I = [-750,-500,-250,0]
recyc_flux = [ 4.2003E+03, 3.2682E+03,  4.5049E+03,  5.4980E+03 ]

s750=Simulation('~/EIM-m750A-v2/P5MW/N4E13D.5_SP4')
Pimp750 = s750['Pimp']

s500=Simulation('~/EIM-m500A-v2/P5MW/N4E13D.5_SP4')
Pimp500 = s500['Pimp']

s250=Simulation('~/EIM-m250A/P5MW/N4E13D.5_C3')
Pimp250 = s250['Pimp']

sstd=Simulation('~/std/P5MW/N4E13D.5_C3')
Pimpstd = sstd['Pimp']

Pimp = [Pimp750.values[-1], Pimp500.values[-1], Pimp250.values[-1], Pimpstd.values[-1] ]


plt.figure()
plt.scatter(PC_I,recyc_flux)
plt.title('Total Recycling Flux as a function of Planar Coil Current')
plt.xlabel('Planar Coil Current (A)')
plt.ylabel('Total Recycling Flux')

plt.figure()
plt.scatter(PC_I,Pimp)
plt.title('Power Losses (Impurities) as a function of Planar Coil Current')
plt.xlabel('Planar Coil Current [A]')
plt.ylabel('Power Losses (Impurities) [MW]')

plt.show()
